package ctx

import "encoding/json"

// JSON decoding function for json files.
func JSON(data []byte, ctx interface{}) error {
	return json.Unmarshal(data, ctx)
}
