package templating

import "bitbucket.org/jatone/tweaker"

// TemplateTasks provides an interface that creates a []tweaker.Task
// given the provided Templaters.
func TemplateTasks(tmpls ...Factory) []tweaker.Task {
	tasks := make([]tweaker.Task, 0, len(tmpls))
	for _, tmpl := range tmpls {
		tasks = append(tasks, TemplateTask(tmpl))
	}
	return tasks
}

// TemplateTask provides an interface that creates a tweaker.Task
// given the Templater.
func TemplateTask(f Factory) tweaker.Task {
	return tasker{f.Templater()}.Task()
}

type tasker struct {
	Templater
}

func (t tasker) Task() tweaker.Task {
	return tweaker.TaskFunc(func() error {
		return t.Templater.Transform()
	})
}
