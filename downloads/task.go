package downloader

import "bitbucket.org/jatone/tweaker"

// DownloadTasks provides an interface that creates tweaker.Task
// given the provided Templater.
func DownloadTasks(f ...Factory) []tweaker.Task {
	tasks := make([]tweaker.Task, 0, len(f))

	for _, factory := range f {
		tasks = append(tasks, DownloadTask(factory.Downloader()))
	}

	return tasks
}

// DownloadTask ...
func DownloadTask(dl Downloader) tweaker.Task {
	return downloadTasker{dl}.Task()
}

type downloadTasker struct {
	Downloader
}

func (t downloadTasker) Task() tweaker.Task {
	return tweaker.TaskFunc(func() error {
		return t.Downloader.Download()
	})
}
