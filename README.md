# Tweaker #

Tweaker is intended to be used for rapid package generation and deployment as part of a continuous deployment pipeline.

It was started as a project to address what I feel are egregious sins with tools like chef/puppet.

It not meant to replace tools like docker, instead it strives to work with such tools, by allowing you to generate the packages for
you application and its system dependencies and letting installation tools configure your container images.

# Goals #
* Unopinionated - Want to generate .deb? .pkg? .rpm? up to you.
* Distribution agnostic. Want to distribute your packages using docker? cron/wget/bash? chef? puppet? torrent/magnetlinks? (see unopinionated)
* WYSIWYG - all outputs are done locally, allowing for visual inspection and debugging.
* Let the package managers handle the heavy lifing such as dependency resolution, file management. They are good at it, let them do their job.
* Promote use of native package managers for system installation.
* Don't require additional to install your application. The Software is already there lets use it!
* Rapid download, package building - lets use our cpu and ram, none of this serial execution.
* Don't promote spewing files all over your servers that then have to be manually cleaned up.
* Rock solid. Its a tool it should make your life easier not harder.

## API Stability/Versioning ##
* Only stable within a minor version. i.e) 1.0.1, 1.0.2 will all be api compatible, 1.0.x and 1.1.x and 1.2.x may not be compatible.
* Odd minor numbers will be development versions.
* Only applies to the core API not to plugins. plugins managed by me will follow similar stability promises.

# Roadmap #
## Features Version 1.0 (Current, WIP): ##
* Support for pacman packages. ✓ [pacman plugin](https://bitbucket.org/jatone/tweaker-pacman/overview)
* Support for debian packages. (in progress)
* Support for git repositories. (in progress)
* Ability to monitor progress of downloads. ✓ (not great yet)
* Ability to request input from user.
* Template based configuration file generation. ✓
* Parallel generation of resources. ✓
* Checkpoint Synchronization (i.e. wait for all these tasks to complete before executing this task) ✓
* Manual compilation required. ✓ (though honestly more of a con than feature. See version 1.2)

## Features Version 1.2: ##
* Improve Usability (based on feedback/experiences)
* Automate away manual compilation of the executable. Investigate options (such as autoloading shared libraries).
* Explore building tool chain to auto build on demand.
* Support More file sources i.e) ftp, torrent, git.
* API based backend. Daemon mode with clients that can connect for status updates
