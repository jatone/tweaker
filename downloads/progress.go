package downloader

import (
	"fmt"
	"sort"
	"sync"
	"sync/atomic"
	"time"

	"github.com/pivotal-golang/bytefmt"
)

type ProgressMonitor func(Progress)

type Progress struct {
	ID        uint64
	Name      string
	Total     uint64
	Completed uint64
}

type inProgress []Progress

func (t inProgress) Len() int      { return len(t) }
func (t inProgress) Swap(i, j int) { t[i], t[j] = t[j], t[i] }

type byID struct{ inProgress }

func (t byID) Less(i, j int) bool { return t.inProgress[i].ID < t.inProgress[j].ID }

type byPercentage struct{ inProgress }

func (t byPercentage) Less(i, j int) bool {
	ratio1 := float64(t.inProgress[i].Completed) / float64(t.inProgress[i].Total)
	ratio2 := float64(t.inProgress[j].Completed) / float64(t.inProgress[j].Total)
	return ratio1 < ratio2
}

// IOProgress - Experimental reports progress of the download.
// Will eventually be incorporated into the tweaker configuration as a general
// api.
type IOProgress struct {
	Progress
	ProgressMonitor
}

func (t *IOProgress) Write(p []byte) (int, error) {
	n := len(p)
	t.Completed += uint64(n)
	t.ProgressMonitor(t.Progress)
	return n, nil
}

func PrintProgress(p Progress) {
	fmt.Printf(
		"%d(%s):%6s/%-.6s, %3.2f%%\n",
		p.ID,
		p.Name,
		bytefmt.ByteSize(p.Completed),
		bytefmt.ByteSize(p.Total),
		float64(p.Completed)/float64(p.Total),
	)
}

type ProgressPrinter struct {
	sync.Mutex
	outstanding map[uint64]Progress
}

func (t *ProgressPrinter) Update(p Progress) {
	t.Mutex.Lock()
	defer t.Mutex.Unlock()

	t.outstanding[p.ID] = p
}

func (t *ProgressPrinter) Output() {
	t.Mutex.Lock()
	defer t.Mutex.Unlock()
	if len(t.outstanding) == 0 {
		return
	}

	fmt.Println("Begin")
	defer fmt.Println("End")

	v := make([]Progress, 0, len(t.outstanding))

	for _, value := range t.outstanding {
		v = append(v, value)
	}

	pp := inProgress(v)
	sort.Sort(sort.Reverse(byPercentage{pp}))
	for _, p := range pp {
		PrintProgress(p)
		if p.Completed == p.Total {
			delete(t.outstanding, p.ID)
		}
	}
}

func NewID() uint64 {
	return atomic.AddUint64(counter, 1)
}

var counter = new(uint64)
var pp = &ProgressPrinter{
	outstanding: make(map[uint64]Progress),
}

var DefaultProgress = pp.Update

func init() {
	fmt.Println("initializer default monitor")
	ticker := time.Tick(time.Second)
	go func() {
		for _ = range ticker {
			pp.Output()
		}
	}()
}
