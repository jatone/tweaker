package tweaker

import (
	"io"
	"os"
)

// MaybeIOWriter wraps an error inside of the io.Writer interface
func MaybeIOWriter(w io.Writer, err error) io.Writer {
	if err != nil {
		return errWriter{err}
	}
	return w
}

// MaybeIOWriteCloser wraps an error inside of the io.WriteCloser interface
func MaybeIOWriteCloser(w io.WriteCloser, err error) io.WriteCloser {
	if err != nil {
		return errWriter{err}
	}

	return w
}

// MaybeIOReadCloser ensures a io.ReadCloser is returned. if the error is not nil
// it returns a special reader that will fail on the first read invocation.
func MaybeIOReadCloser(r io.ReadCloser, err error) io.ReadCloser {
	if err != nil {
		return errReader{err}
	}

	return r
}

type errReader struct {
	err error
}

func (t errReader) Read(_ []byte) (int, error) {
	return 0, t.err
}

func (t errReader) Close() error {
	return nil
}

type errWriter struct {
	err error
}

func (t errWriter) Write(_ []byte) (int, error) {
	return 0, t.err
}

func (t errWriter) Close() error {
	return nil
}

// Copy the src file to the dst file, uses the provided permissions.
func Copy(src, dst string, perm os.FileMode) Task {
	return TaskFunc(func() error {
		dstIO := MaybeIOWriteCloser(os.OpenFile(dst, os.O_CREATE|os.O_RDWR|os.O_TRUNC, perm))
		defer dstIO.Close()
		srcIO := MaybeIOReadCloser(os.Open(src))
		defer srcIO.Close()

		_, err := io.Copy(dstIO, srcIO)
		return err
	})
}
