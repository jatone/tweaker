package ctx

// Common code for loading data from yml files and
// combining them with templates.
// ld := ctx.NewLoader(ctx.YAML).From(path).Into(details{}).Build()

// DecoderFunc function that will transform the raw bytes into
// the provided struct.
type DecoderFunc func([]byte, interface{}) error

// Decoder provides the decode function that will perform the decoding.
type Decoder interface {
	Decode() (interface{}, error)
}

// Factory for configuring and building the decoder.
type Factory interface {
	Into(ctx interface{}) Factory
	From(path string) Factory
	Build() Decoder
}
