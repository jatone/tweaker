package templating

import "text/template"

// NewTemplateCache create a cache from the provided template.
func NewTemplateCache(t *template.Template) Cache {
	return cache{t}
}

// NewCache builds a template cache from the provided path/pattern.
func NewCache(glob string) (Cache, error) {
	templates, err := template.ParseGlob(glob)
	return cache{templates}, err
}

type cache struct {
	template *template.Template
}

func (t cache) Template(name string) Factory {
	return factory{
		templater{
			template: t.template.Lookup(name),
		},
	}
}

// Special cache for delaying error handling.
type errCache struct {
	errFactory
}

// Template will always return an errTemplater
func (t errCache) Template(name string) Factory {
	return t.errFactory
}
