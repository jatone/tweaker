package templating

// Must panics if there is an error
func Must(cache Cache, err error) Cache {
	if err != nil {
		panic(err)
	}

	return cache
}

// MaybeCache returns a special Cache implementation that errors
// out on any method invocations.
func MaybeCache(cache Cache, err error) Cache {
	if err != nil {
		return errCache{errFactory{errTemplater{err}}}
	}

	return cache
}
