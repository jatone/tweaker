package templating

import (
	"io"
	"os"
	"text/template"

	"bitbucket.org/jatone/tweaker/context"
)

// Provides common code for dealing with templates and data.
// cache := templating.MaybeCache(templating.NewCache("path/pattern"))
// factory := cache.Template("my-template").
// 	WithCtx(&struct{}).
// 	TransformToFile("path", 0755)
// templating.TemplateTasks(factory.Templater())

// Cache thin wrapper around text/template
// providing access to only the lookup method.
type Cache interface {
	Template(name string) Factory
}

// Factory interface for building Templater structures.
type Factory interface {
	WithCtx(interface{}) Factory
	WithCtxFromFile(fmt ctx.DecoderFunc, path string, dst interface{}) Factory
	FuncMap(f template.FuncMap) Factory
	// TransformToFile is a convience method that delegates down
	// to TransformTo
	TransformToFile(path string, perm os.FileMode) Factory
	TransformTo(dst io.WriteCloser) Factory
	Templater() Templater
}

// Templater interface for rendering a template with the provided context.
type Templater interface {
	Transform() error
}
