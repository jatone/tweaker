package tweaker

import "os"

// MultiMkdir utility function to make many directories in a single call.
func MultiMkdir(mode os.FileMode, directory ...string) error {
	for _, dir := range directory {
		if err := os.MkdirAll(dir, mode); err != nil {
			return err
		}
	}

	return nil
}

func Truncate(s string, max int) string {
	if len(s) > max {
		return s[:max]
	}

	return s
}
