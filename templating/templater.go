package templating

import (
	"io"
	"text/template"

	"bitbucket.org/jatone/tweaker/context"
)

type templater struct {
	template *template.Template
	dst      io.WriteCloser
	decoder  ctx.Decoder
}

func (t templater) Transform() error {
	ctx, err := t.decoder.Decode()
	if err != nil {
		return err
	}
	return t.template.Execute(t.dst, ctx)
}

type errTemplater struct {
	err error
}

func (t errTemplater) Transform() error {
	return t.err
}
