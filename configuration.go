package tweaker

// Configuration tweaker configuration contains a compiler factory and
// a working directory.
type Configuration struct {
	WorkingDirectory Pather
	CompilerFactory
}

// Path see Pather interface
func (t Configuration) Path(parts ...string) string {
	return t.WorkingDirectory.Path(parts...)
}

// Pather see Pather interface
func (t Configuration) Pather(parts ...string) Pather {
	return t.WorkingDirectory.Pather(parts...)
}

// BaseDir see Pather interface
func (t Configuration) BaseDir() string {
	return t.WorkingDirectory.BaseDir()
}
