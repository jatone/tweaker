package downloader

import (
	"fmt"
	"io"
	nethttp "net/http"
	"net/url"
	"os"

	"bitbucket.org/jatone/tweaker"
)

var HTTP = HTTPFactory(httpFactory{
	http{
		ProgressMonitor: DefaultProgress,
	},
})

type HTTPFactory interface {
	Factory
	URL(url string) HTTPFactory
	DownloadTo(path string, perm os.FileMode) HTTPFactory
}

type httpFactory struct {
	http
}

func (t httpFactory) URL(url string) HTTPFactory {
	t.http.URL = url
	return httpFactory{t.http}
}

func (t httpFactory) DownloadTo(path string, perm os.FileMode) HTTPFactory {
	t.http.Buffer = tweaker.MaybeIOWriteCloser(os.OpenFile(path, os.O_CREATE|os.O_RDWR|os.O_TRUNC, perm))
	return httpFactory{t.http}
}

func (t httpFactory) Downloader() Downloader {
	return t.http
}

type http struct {
	URL    string
	Buffer io.WriteCloser
	ProgressMonitor
}

func (t http) Download() error {
	var err error
	var uri *url.URL
	defer t.Buffer.Close()

	if uri, err = url.Parse(t.URL); err != nil {
		return err
	}

	if uri.Scheme != "http" && uri.Scheme != "https" {
		return fmt.Errorf("invalid scheme expected: http(s)")
	}

	resp, err := nethttp.Get(uri.String())
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != nethttp.StatusOK {
		return fmt.Errorf("Status code not 200")
	}

	pw := &IOProgress{
		Progress: Progress{
			ID:    NewID(),
			Name:  uri.String(),
			Total: uint64(resp.ContentLength),
		},
		ProgressMonitor: t.ProgressMonitor,
	}

	dst := io.MultiWriter(t.Buffer, pw)
	n, err := io.Copy(dst, resp.Body)
	if n != resp.ContentLength && err == nil {
		return fmt.Errorf("length != downloaded: (%d!=%d)", resp.ContentLength, n)
	}

	return err
}
