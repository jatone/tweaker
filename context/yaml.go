package ctx

import "gopkg.in/yaml.v2"

// YAML decoding function for yaml files.
func YAML(data []byte, ctx interface{}) error {
	return yaml.Unmarshal(data, ctx)
}
