package ctx

import "io/ioutil"

// NewCtx wraps an random structure into a Decoder
func NewCtx(context interface{}) Decoder {
	return wrappedCtx{context}
}

// NewDecoder creates a new decoder than decode the contents of the
// source file with the specified function.
func NewDecoder(d DecoderFunc) Factory {
	return factory{decoder{DecoderFunc: d}}
}

type wrappedCtx struct {
	context interface{}
}

func (t wrappedCtx) Decode() (interface{}, error) {
	return t.context, nil
}

type decoder struct {
	from    string
	context interface{}
	DecoderFunc
}

func (t decoder) Decode() (interface{}, error) {
	var err error
	var data []byte

	if data, err = ioutil.ReadFile(t.from); err != nil {
		return t.context, err
	}

	return t.context, t.DecoderFunc(data, t.context)
}

type factory struct {
	decoder
}

func (t factory) Into(ctx interface{}) Factory {
	t.context = ctx
	return t
}

func (t factory) From(path string) Factory {
	t.from = path
	return t
}

func (t factory) Build() Decoder {
	return t.decoder
}
