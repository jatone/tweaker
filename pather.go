package tweaker

import "path/filepath"

// NewPather returns a Pather rooted at the provided path.
func NewPather(path string) Pather {
	return pather(path)
}

type pather string

func (t pather) Path(parts ...string) string {
	return filepath.Join(append([]string{string(t)}, parts...)...)
}

func (t pather) Pather(parts ...string) Pather {
	return NewPather(t.Path(parts...))
}

func (t pather) BaseDir() string {
	return string(t)
}
