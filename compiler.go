package tweaker

import (
	"log"
	"os"
	"sync"
	"sync/atomic"
)

// NewCompilerFactory creates a factory for building compiler's and sets the pipe
// to insert tasks into.
func NewCompilerFactory(pipe chan ExecutableTask) CompilerFactory {
	return CompilerFactory(compilerFactory{
		compiler{
			pipe: pipe,
			Log:  log.New(os.Stderr, log.Prefix(), log.LstdFlags),
			lock: new(int32),
		},
	})
}

type compiler struct {
	name string
	Log  *log.Logger
	pipe chan ExecutableTask
	lock *int32
}

func (t compiler) Compile(tasks ...Task) {
	// short circuit compile if we've seen more than 1 error
	if atomic.LoadInt32(t.lock) > 0 {
		t.Log.Println("refusing to compile tasks due to previous errors")
		return
	}

	t.Log.Println("compiling", len(tasks), "tasks")
	defer t.Log.Println("compilation completed")

	errorHandleCallback := func(err error) {
		atomic.AddInt32(t.lock, 1)
		t.Log.Println(t.name, err)
	}

	wg := &sync.WaitGroup{}
	wg.Add(len(tasks))

	for _, task := range tasks {
		t.pipe <- ExecutableTask{
			WaitGroup:    wg,
			name:         t.name,
			errorHandler: errorHandleCallback,
			Task:         task,
		}
	}

	t.Log.Println("Waiting for completion")
	wg.Wait()
}

type compilerFactory struct {
	compiler
}

func (t compilerFactory) Name(n string) CompilerFactory {
	t.compiler.name = n
	return CompilerFactory(compilerFactory{t.compiler})
}

func (t compilerFactory) Logger(l *log.Logger) CompilerFactory {
	t.compiler.Log = l
	return CompilerFactory(compilerFactory{t.compiler})
}

func (t compilerFactory) Compiler() Compiler {
	return Compiler(t.compiler)
}
