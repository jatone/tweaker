package tweaker

import "log"

// Pather interface for building out paths from parent directories.
type Pather interface {
	Path(parts ...string) string
	Pather(parts ...string) Pather
	BaseDir() string
}

// Definition defines units of work
type Definition interface {
	Execute(Configuration)
}

// DefinitionFunc allows pure function implementations of Definition
type DefinitionFunc func(Configuration)

// Execute execute unit of work
func (t DefinitionFunc) Execute(c Configuration) {
	t(c)
}

// Task individual task to execute
type Task interface {
	Execute() error
}

// TaskFunc allows pure function implementations of Tasks
type TaskFunc func() error

// Execute individual task to execute
func (t TaskFunc) Execute() error {
	return t()
}

// CompilerFactory builder to build a task compiler
type CompilerFactory interface {
	Compiler() Compiler
	Name(name string) CompilerFactory
	Logger(*log.Logger) CompilerFactory
}

// Compiler executes tasks
type Compiler interface {
	Compile(tasks ...Task)
}
