package tweaker

import (
	"log"
	"runtime"
	"sync"
)

// ExecutableTask monitors the currently executing tasks and is responsible
// for waiting for them to complete.
type ExecutableTask struct {
	*sync.WaitGroup
	Task
	name         string
	errorHandler func(error)
}

// Execute the underlying task.
func (t ExecutableTask) Execute() (err error) {
	defer func() {
		if r := recover(); r != nil {
			if _, ok := r.(runtime.Error); ok {
				panic(r)
			}
			err = r.(error)
		}
	}()

	return t.Task.Execute()
}

// Executer given an ID and a channel to pull tasks from for execution.
func Executer(i int, input chan ExecutableTask) {
	log.Println("Awaiting tasks")
	for task := range input {
		log.Println("Executing(", i, ")", task.name)
		if err := task.Execute(); err != nil {
			task.errorHandler(err)
		}
		task.WaitGroup.Done()
	}
}
