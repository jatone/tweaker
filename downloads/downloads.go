package downloader

import "bitbucket.org/jatone/tweaker"

// df := downloader.HTTP.
//		URL("https://storage.googleapis.com/golang/go1.5.1.linux-amd64.tar.gz")
// dl := downloader.DownloadTasks(
// 		df.DownloadTo(downloads.Path("go.1"), 0755),
// 		df.DownloadTo(downloads.Path("go.2"), 0755),
// )

// Downloader interface describing functions for performing downloads.
type Downloader interface {
	// Download downloads the specified content based on the underlying downloader.
	Download() error
}

// Factory ...
type Factory interface {
	Downloader() Downloader
}

// Tasker interface for creating download tasks.
type Tasker interface {
	// Task builds a task that performs a download when executed.
	Task() tweaker.Task
}
