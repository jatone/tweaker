package archive

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
)

func ExtractTar(archive, dst string) error {
	var err error
	var f *os.File

	if f, err = os.Open(archive); err != nil {
		return err
	}

	r, err := gzip.NewReader(f)
	if err != nil {
		return err
	}

	tr := tar.NewReader(r)
	for {
		hdr, err := tr.Next()
		if err != nil {
			switch err {
			case io.EOF:
				return nil
			default:
				return err
			}
		}

		path := filepath.Join(dst, hdr.Name)
		if hdr.FileInfo().IsDir() {
			if err := os.Mkdir(path, os.FileMode(hdr.Mode)); err != nil {
				return err
			}
			continue
		}

		o, err := os.Create(path)
		if err != nil {
			return err
		}
		if _, err := io.Copy(o, tr); err != nil {
			return err
		}
	}
}

func NewTarArchive(archive string) Archive {
	return Archive{
		Archive: archive,
	}
}

// Archive task for building a file from a template
// and placing it into at a location within the file system.
type Archive struct {
	Archive string
}

// Build builds a tar archive inside of the buildDirectory.
func (t Archive) Build(srcdir string, paths ...string) (err error) {
	f, err := os.Create(t.Archive)
	if err != nil {
		return err
	}
	defer func() {
		if err = f.Close(); err != nil {
			fmt.Println("error closing file", err)
		}
	}()

	tarArchive := tar.NewWriter(f)
	defer func() {
		if err = tarArchive.Close(); err != nil {
			fmt.Println("error closing tar archive", err)
		}
	}()

	// TODO use Walk
	for _, p := range paths {
		var err error
		var file *os.File
		var fi os.FileInfo
		_p := filepath.Join(srcdir, p)

		if file, err = os.Open(_p); err != nil {
			fmt.Println("error", err)
			return err
		}
		defer file.Close()

		if fi, err = file.Stat(); err != nil {
			return err
		}

		h, err := tar.FileInfoHeader(fi, "")
		if err != nil {
			return err
		}

		h.Name = p
		if err = tarArchive.WriteHeader(h); err != nil {
			return err
		}

		if fi.IsDir() {
			continue
		}

		if _, err = io.Copy(tarArchive, file); err != nil {
			return err
		}
	}

	return tarArchive.Flush()
}

// Helper func for creating parent directory.
func create(filepath string) (file *os.File, err error) {
	if err = os.MkdirAll(path.Dir(filepath), 0750); err != nil {
		return
	}
	return os.Create(filepath)
}
