package main

import (
	"log"
	"os"
	"text/template"

	"bitbucket.org/jatone/tweaker"
	"bitbucket.org/jatone/tweaker/templating"
)

var logger = log.New(os.Stderr, "tweaker-example ", log.LstdFlags)

func main() {
	workingDirectory, err := os.Getwd()
	if err != nil {
		log.Fatalln(err)
	}

	pipe := make(chan tweaker.ExecutableTask)
	for i := 0; i < 5; i++ {
		go tweaker.Executer(i, pipe)
	}

	configuration := tweaker.Configuration{
		WorkingDirectory: tweaker.NewPather(workingDirectory),
		CompilerFactory:  tweaker.NewCompilerFactory(pipe),
	}

	definition1.Execute(configuration)
	definition2.Execute(configuration)
}

var definition1 = tweaker.DefinitionFunc(func(configuration tweaker.Configuration) {
	cFactory := configuration.CompilerFactory.Name("tweaker-example-1").Logger(logger)
	compiler := cFactory.Logger(logger).Compiler()

	compiler.Compile(printMeTask("Starting Task Definition"))
	compiler.Compile(
		printMeTask("Parallel Task 1"),
		printMeTask("Parallel Task 2"),
		printMeTask("Parallel Task 3"),
		printMeTask("Parallel Task 4"),
	)
	compiler.Compile(printMeTask("Serial Task 5"))
	compiler.Compile(
		printMeTask("Parallel Task 6"),
		printMeTask("Parallel Task 7"),
		printMeTask("Parallel Task 8"),
		printMeTask("Parallel Task 9"),
	)
	compiler.Compile(printMeTask("Serial Task A"))
	compiler.Compile(
		printMeTask("Parallel Task B"),
		printMeTask("Parallel Task C"),
		printMeTask("Parallel Task D"),
		printMeTask("Parallel Task E"),
	)
	compiler.Compile(printMeTask("Serial Task F"))
	compiler.Compile(printMeTask("Done Task Definition"))
})

var definition2 = tweaker.DefinitionFunc(func(config tweaker.Configuration) {
	cFactory := config.CompilerFactory.Name("tweaker-example-2").Logger(logger)
	compiler := cFactory.Logger(logger).Compiler()

	tmpl := templating.NewTemplateCache(template.Must(
		template.New("simple-template").Parse(rawSimpleTemplate),
	))
	buildDir := config.Pather("build")
	apacheDir := buildDir.Pather("apache")
	postgresDir := buildDir.Pather("postgres")

	err := tweaker.MultiMkdir(
		0755,
		buildDir.BaseDir(), apacheDir.BaseDir(), postgresDir.BaseDir(),
		apacheDir.Path("resource"), postgresDir.Path("resource"),
	)

	if err != nil {
		logger.Println("failed creating directories, bailing out", err)
		return
	}

	tmpl1 := tmpl.Template("simple-template").
		WithCtx(simpleTemplateContext{Name: "World"}).
		TransformToFile(apacheDir.Path("resource", "example"), 0755)

	tmpl2 := tmpl.Template("simple-template").
		WithCtx(simpleTemplateContext{Name: "World"}).
		TransformToFile(postgresDir.Path("resource", "example"), 0755)

	compiler.Compile(templating.TemplateTasks(tmpl1, tmpl2)...)
})

func die(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func printMeTask(msg string) tweaker.TaskFunc {
	return tweaker.TaskFunc(func() error {
		logger.Println(msg)
		return nil
	})
}

var rawSimpleTemplate = `Hello {{.Name}}`

type simpleTemplateContext struct {
	Name string
}
