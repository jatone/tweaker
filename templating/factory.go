package templating

import (
	"io"
	"os"
	"text/template"

	"bitbucket.org/jatone/tweaker"
	"bitbucket.org/jatone/tweaker/context"
)

type factory struct {
	templater
}

func (t factory) WithCtx(context interface{}) Factory {
	t.templater.decoder = ctx.NewCtx(context)
	return factory{t.templater}
}

func (t factory) WithCtxFromFile(fmt ctx.DecoderFunc, path string, dst interface{}) Factory {
	t.templater.decoder = ctx.NewDecoder(fmt).From(path).Into(dst).Build()
	return factory{t.templater}
}

func (t factory) TransformTo(dst io.WriteCloser) Factory {
	t.templater.dst = dst
	return factory{t.templater}
}

func (t factory) TransformToFile(path string, perm os.FileMode) Factory {
	return t.TransformTo(
		tweaker.MaybeIOWriteCloser(os.OpenFile(path, os.O_CREATE|os.O_RDWR|os.O_TRUNC, perm)),
	)
}

func (t factory) FuncMap(f template.FuncMap) Factory {
	var err error
	t.templater.template, err = t.templater.template.Clone()
	if err != nil {
		return errFactory{errTemplater{err}}
	}
	t.templater.template.Funcs(f)
	return t
}

func (t factory) Templater() Templater {
	return t.templater
}

type errFactory struct {
	errTemplater
}

func (t errFactory) WithCtx(context interface{}) Factory {
	return t
}

func (t errFactory) WithCtxFromFile(fmt ctx.DecoderFunc, path string, dst interface{}) Factory {
	return t
}

func (t errFactory) TransformTo(io.WriteCloser) Factory {
	return t
}

func (t errFactory) TransformToFile(path string, perm os.FileMode) Factory {
	return t
}

func (t errFactory) FuncMap(f template.FuncMap) Factory {
	return t
}

func (t errFactory) Templater() Templater {
	return t.errTemplater
}
